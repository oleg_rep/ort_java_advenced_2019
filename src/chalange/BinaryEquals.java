package chalange;

public class BinaryEquals {

	private int reverseToBinary(int teensDigit) {
		int x = 0;
		StringBuilder result = new StringBuilder();

		do {
			x = teensDigit % 2;
			teensDigit /= 2;
			result.append(x);
		} while (teensDigit != 0);

		return Integer.parseInt(result.toString(), 10);
	}

	public boolean equalsBynary(int a, int b) {
		int x = reverseToBinary(a);
		int y = reverseToBinary(b);
		return x == y;
	}
}
