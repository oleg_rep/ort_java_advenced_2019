package chalange;

import java.util.ArrayList;
import java.util.List;

public class CollectionsEquality {

	private List<Integer> toChars(int digit) {
		List<Integer> intList = new ArrayList<>();
		while (digit != 0) {
			int n = digit % 10;
			digit /= 10;
			intList.add(n);
		}

		return intList;
	}

	public boolean listContains(int a, int b) {
		boolean result = false;
		List<Integer> x = toChars(a);
		List<Integer> y = toChars(b);

		return x.containsAll(y);
	}

	public boolean listEquals(int a, int b) {
		boolean result = false;
		List<Integer> x = toChars(a);
		List<Integer> y = toChars(b);

		return x.equals(y);
	}
}
