package chalange;

public class EqualsServices {
	/*
	 * Вычисляет разницу между цифрами, если её нет - то равны
	 */
	public boolean minusInt(int a, int b) {
		return a - b == 0;
	}

	/*
	 * Сравнение через equals
	 */
	public boolean equalsInt(Integer a, Integer b) {
		return a.equals(b);
	}

	/*
	 * Складываются 2 числа и делется пополам, если результат равен и а и б - true
	 */
	public boolean mathEquality(int a, int b) {
		int tmp = (a + b) / 2;
		return (a == tmp && b == tmp);
	}

	/*
	 * Берёт 1-е число и сравнивает побитово со 2-м образуя 3-е число. Если биты
	 * одинаковые - то возвращает 0, если разные - то 1. В итоге получается, что если
	 * проверяемые числа разные - то образует 3-е число. Если одинаковые - то 3-е
	 * число будет = 0. Ну и таким образом получается если a^b == 0 - то они
	 * одинаковые
	 */

	public boolean byteEquality(int a, int b) {
		return (a ^ b) == 0;
	}

	/*
	 * Проверка при помощи деления по модулю. Данные собираются в коллекцию и
	 * проверяются, если все 0, то true,иначе false (более подробное описание в
	 * классе ModEquality)
	 * 
	 */

	public void modEqualty(int a, int b) {
		ModEquality equality = new ModEquality(a, b);
		equality.modEqualityPrint();
	}

	/*
	 * Сравнивает число в двоичной системе счисления. Принимает 2 числа, переводит в
	 * 2-ю систему и сравнивает в ней
	 * 
	 */
	public boolean binaryEquals(int a, int b) {
		BinaryEquals bin = new BinaryEquals();
		return bin.equalsBynary(a, b);
	}

	/*
	 * Сравнивает через компаратор. Если метод compare возвращает 0 - то равны
	 */
	public boolean compareEquality(int a, int b) {
		IntComparator comparator = new IntComparator();
		int eq = comparator.compare(a, b);
		return eq == 0;
	}

	CollectionsEquality lists = new CollectionsEquality();

	/*
	 * Принимает 2 числа, разбивает их и ложит в коллекцию (List), потом сравнивает
	 * обе коллекции через equals
	 */
	public boolean listEquality(int a, int b) {
		return lists.listEquals(a, b);
	}

	/*
	 * Принимает 2 числа, разбивает их и ложит в коллекцию(List), потом сравнивает
	 * обе коллекции через contans
	 */

	public boolean listContans(int a, int b) {
		return lists.listContains(a, b);
	}
}