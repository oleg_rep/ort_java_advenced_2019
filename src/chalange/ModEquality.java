package chalange;

import java.util.ArrayList;
import java.util.List;

public class ModEquality {

	private int a;
	private int b;

	ModEquality(int a, int b) {
		this.a = a;
		this.b = b;
	}

	/*
	 * счетчик количества цифер в числе, в дальнейшем по нему определяется, стоит-ли
	 * дальше производить расчет в методе modEquals. 
	 * */
	int countDigit(int x) {
		int count = 0;

		while (x % 10 > 0) {
			count++;
			x /= 10;
		}

		return count;
	}

	
	
	/*
	 * сравнение по модулю. С помощью метода countDigit считаем количество чисел, проверяем если длина чисел не равна - false. 
	 * Если равно, то отрезаем по одной цифре с конца у обеихз чисел и сравниваем, если равны - то добавляем 0 в List иначе 1
	 * */
	private List modEquals() {
		List<Integer> checker = new ArrayList<>();

		int checkA = 0;
		int checkB = 0;
		int lengthDigitA = countDigit(a);
		int lengthDigitB = countDigit(b);

		if (lengthDigitA != lengthDigitB) {
			checker.add(1);
		} else {
			while (a > 0) {
				checkA = a % 10;
				checkB = b % 10;

				if (checkA == checkB) {
					checker.add(0);
				} else {
					checker.add(1);
				}

				a /= 10;
				b /= 10;
			}
		}
		return checker;
	}

	
	
	
	/*
	 * Собственно проверка. Если есть хоть 1-на еденица - false, иначе true
	 * */
	private boolean checkEquality(List<Integer> checker) {
		boolean result = true;

		for (Integer check : checker) {
			if (check == 1) {
				result = false;
			}
		}
		return result;
	}

	public void modEqualityPrint() {
		System.out.println("Module equality " + a + " and " + b + ": ");
		List checker = modEquals();
		boolean eq = checkEquality(checker);

		if (eq == true) {
			System.out.println("Digits is equals");
		} else {
			System.out.println("Digits isn't equals");
		}
	}
}
