package com.ort.javaadvanced2019.task_7;

import com.ort.javaadvanced2019.task_7.services.CalculatorService;
import com.ort.javaadvanced2019.task_7.services.Calculator;
import com.ort.javaadvanced2019.task_7.services.ExempleReader;
import com.ort.javaadvanced2019.task_7.services.ExempleWriter;

public class AppMain {

	public static void main(String[] args) throws InterruptedException {

		ExempleReader reader = new ExempleReader("src/resources/exemples.txt");
		ExempleWriter writer = new ExempleWriter("src/resources/exemplesAnswers.txt");
		Calculator calculate = new Calculator();
		CalculatorService calculatorService = new CalculatorService(reader, writer, calculate);

		StringBuilder data = new StringBuilder();
		Thread t1 = new Thread(calculatorService.writeToFileService(0, reader.parseExemple().size() / 2, data));
		t1.start();
		Thread t2 = new Thread(
				calculatorService.writeToFileService(reader.parseExemple().size() / 2, reader.parseExemple().size(), data));
		t2.start();

		
		t1.join();
        t2.join();
        writer.writeUsingFiles(data.toString());
		System.out.println(data.toString());
	}
}
