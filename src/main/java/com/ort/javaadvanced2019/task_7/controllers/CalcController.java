package com.ort.javaadvanced2019.task_7.controllers;

import com.ort.javaadvanced2019.task_7.entities.Exemple;
import com.ort.javaadvanced2019.task_7.utils.Calculate;
import com.ort.javaadvanced2019.task_7.utils.ExempleReader;
import com.ort.javaadvanced2019.task_7.utils.ExempleWriter;

public class CalcController {

	private ExempleReader reader;
	private ExempleWriter writer;
	private Calculate calculate;

	public CalcController(ExempleReader reader, ExempleWriter writer, Calculate calculate) {
		this.reader = reader;
		this.writer = writer;
		this.calculate = calculate;
	}

	public void writeController() {
		StringBuilder data = new StringBuilder();
		for (Exemple e : reader.parseExemple()) {
			data.append(e).append(" = ").append(calculate.calc(e.getA(), e.getB(), e.getCharacter())).append("\n");
		}
		writer.writeUsingFiles(data.toString());
	}
}
