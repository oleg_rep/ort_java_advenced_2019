package com.ort.javaadvanced2019.task_7.entities;

public class Exemple {
	private int a;
	private char character;
	private int b;

	public Exemple(int a, char character, int b) {
		this.a = a;
		this.character = character;
		this.b = b;
	}

	public int getA() {
		return a;
	}

	public char getCharacter() {
		return character;
	}

	public int getB() {
		return b;
	}

	@Override
	public String toString() {
		return a + " " + character + " " + b;
	}

}
