package com.ort.javaadvanced2019.task_7.services;

public class Calculator {

	public int calc(int a, int b, char character) {

		int result = 0;

		switch (character) {
		case '+': {
			result = (a + b);
			break;
		}
		case '*': {
			result = a * b;
			break;
		}
		case '-': {
			result = a - b;
			break;
		}
		case '/': {
			result = a / b;
			break;
		}
		default:
			break;
		}
		return result;
	}
}
