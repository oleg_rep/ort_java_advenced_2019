package com.ort.javaadvanced2019.task_7.services;

import com.ort.javaadvanced2019.task_7.entities.Exemple;

public class CalculatorService {

	private ExempleReader reader;
	private ExempleWriter writer;
	private Calculator calculate;

	public CalculatorService(ExempleReader reader, ExempleWriter writer, Calculator calculate) {
		this.reader = reader;
		this.writer = writer;
		this.calculate = calculate;
	}

	public Runnable writeToFileService(int start, int end, StringBuilder data) {
		Exemple[] exemples = reader.parseExemple().toArray(new Exemple[reader.parseExemple().size()]);

		Runnable run = () -> {
			for (int i = start; i < end; i++) {
				data.append(exemples[i]).append(" = ")
						.append(calculate.calc(exemples[i].getA(), exemples[i].getB(), exemples[i].getCharacter()))
						.append("\n");
			}
		};
		return run;
	}
}