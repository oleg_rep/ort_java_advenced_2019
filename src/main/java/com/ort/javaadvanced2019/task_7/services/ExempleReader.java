package com.ort.javaadvanced2019.task_7.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.ort.javaadvanced2019.task_7.entities.Exemple;

public class ExempleReader {

    private String path;

    public ExempleReader(String path) {
        this.path = path;
    }

    private List<String> readFile() {
        List<String> dataFromFile = null;
        try (Scanner calc = new Scanner(new File(path))) {
            dataFromFile = new LinkedList<>();
            while (calc.hasNextLine()) {
                dataFromFile.add(calc.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return dataFromFile;
    }

    public List<Exemple> parseExemple() {
        List<String> calc = readFile();
        List<Exemple> exemples = new LinkedList<>();

        for (String s : calc) {
            try {
                String[] strArray = s.split("\\s");
                int a = Integer.parseInt(strArray[0]);
                char c = strArray[1].charAt(0);
                int b = Integer.parseInt(strArray[2]);
                exemples.add(new Exemple(a, c, b));
            } catch (ArrayIndexOutOfBoundsException e) {
                System.err.println("File format exception (Fix your file)");
            }
        }
        return exemples;
    }
}
