package com.ort.javaadvanced2019.task_7.utils;

public class Calculate {

	ExempleReader exempleReader;

	public Calculate(ExempleReader exempleReader) {
		this.exempleReader = exempleReader;
	}

	public int calc(int a, int b, char character) {

		int result = 0;

		switch (character) {
		case '+': {
			result = (a + b);
			break;
		}
		case '*': {
			result = a * b;
			break;
		}
		case '-': {
			result = a - b;
			break;
		}
		case '/': {
			result = a / b;
			break;
		}
		default:
			break;
		}
		return result;
	}

}
