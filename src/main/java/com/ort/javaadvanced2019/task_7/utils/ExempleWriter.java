package com.ort.javaadvanced2019.task_7.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExempleWriter {
	private String path;

	public ExempleWriter(String path) {
		this.path = path;
	}

	public void writeUsingFiles(String data) {
		try {
			Files.write(Paths.get(path), data.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}