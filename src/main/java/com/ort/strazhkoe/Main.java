package com.ort.strazhkoe;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.dao.impl.FilePointDAO;
import com.ort.strazhkoe.services.PointServiceImpl;
import com.ort.strazhkoe.services.converters.CSVPointConverter;
import com.ort.strazhkoe.services.converters.PointConverter;
import com.ort.strazhkoe.ui.UI;
import com.ort.strazhkoe.ui.swing.SwingUI;

public class Main {
	public static void main(String[] args) {
		PointConverter converter = new CSVPointConverter();
		PointDAO pointDAO = new FilePointDAO("points.csv", converter);
		PointServiceImpl pointService = new PointServiceImpl(pointDAO);
		UI ui = new SwingUI(pointService);
		//********* Loading
		ui.start(args);
	}
}
