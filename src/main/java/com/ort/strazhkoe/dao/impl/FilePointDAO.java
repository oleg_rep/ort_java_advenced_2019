package com.ort.strazhkoe.dao.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.converters.CSVPointConverter;
import com.ort.strazhkoe.services.converters.PointConverter;

public class FilePointDAO implements PointDAO {

	private final String _fileName;
	private final PointConverter _pointConverter;

	public FilePointDAO(String fileName, PointConverter pointConverter) {
		_fileName = fileName;
		_pointConverter = pointConverter;
	}

	@Override
	public List<Point> getAll() {
		List<Point> infoFromFile = new ArrayList<>();

		String line;
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(_fileName)))) {
			while ((line = reader.readLine()) != null) {
				infoFromFile.add(_pointConverter.toPoint(line));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return infoFromFile;
	}

	@Override
	public void saveAll(List<Point> points) {
		try (PrintWriter writer = getWriter()) {
			points.stream().map(_pointConverter::toString).forEach(writer::println);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	protected PrintWriter getWriter() throws IOException {
		return new PrintWriter(new FileWriter(_fileName));
	}

	protected BufferedReader getReader() throws IOException {
		return new BufferedReader(new FileReader(new File(_fileName)));
	}
}
