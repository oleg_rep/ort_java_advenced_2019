package com.ort.strazhkoe.entities;

public class Point {
	private final int _x;
	private final int _y;
	private final String _name;
	
	public Point(final int x, 
			     final int y, 
			     final String name) {
		_x = x;
		_y = y;
		_name = name;
	}

	public int getX() {
		return _x;
	}

	public int getY() {
		return _y;
	}

	public String getName() {
		return _name;
	}
	
	public String toString() {
		return String.format("%s (%d, %d)", _name, _x, _y);
	}
}
