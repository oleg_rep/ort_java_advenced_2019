package com.ort.strazhkoe.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.entities.Point;

public class PointServiceImpl implements PointService {

	private final PointDAO _pointDAO;
	private final List<Point> _data;

	public PointServiceImpl(final PointDAO pointDAO) {
		_pointDAO = pointDAO;
		_data = new ArrayList<>();
//		_data.addAll(Arrays.asList(new Point(1, 2, "A"), 
//				                   new Point(7, 5, "B")));
		_data.addAll(_pointDAO.getAll());
	}

	@Override
	public List<Point> getAll() {
		return Collections.unmodifiableList(_data);
	}

	@Override
	public void saveAll(List<Point> points) {
		_pointDAO.saveAll(points);
	}

	@Override
	public void add(Point point) {
		_data.add(point);
		save();
	}

	@Override
	public void delete(int index) {
		_data.remove(index);
		save();
	}

	@Override
	public void delete(Point point) {
		_data.remove(point);
		save();
	}

	private void save() {
		_pointDAO.saveAll(_data);
	}

}
