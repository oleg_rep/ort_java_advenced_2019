package com.ort.strazhkoe.services.converters;

import com.ort.strazhkoe.entities.Point;

public class CSVPointConverter implements PointConverter {

	private String SEPARATOR = ";";

	@Override
	public Point toPoint(String text) {
		String[] parseText = text.split(SEPARATOR);
		int x = Integer.parseInt(parseText[0]);
		int y = Integer.parseInt(parseText[1]);
		String name = parseText[2];
		return new Point(x, y, name);
	}

	@Override
	public String toString(Point point) {
		StringBuilder sb = new StringBuilder();
		sb.append(point.getX()).append(SEPARATOR).append(point.getY()).append(SEPARATOR).append(point.getName());

		return sb.toString();
	}
}
