package com.ort.strazhkoe.ui.swing;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.PointService;
import com.ort.strazhkoe.ui.UI;

public class SwingUI implements UI {

	private PointService _pointService;
	private DefaultListModel<Point> _listModel;

	public SwingUI(PointService pointService) {
		_pointService = pointService;
	}

	@Override
	public void start(String[] args) {
		JFrame jFrame = new JFrame("Point Manager");
		jFrame.setSize(300, 500);
		jFrame.setLocationRelativeTo(null);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		_listModel = new DefaultListModel<>();

		refresh();

		JList<Point> jList = new JList<>(_listModel);
		jList.setFont(new Font("Serif", Font.PLAIN, 28));

		JPanel buttonPanel = new JPanel();
		JButton addButton = new JButton("Add Point");

		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AddPointDialog addPointDialog = new AddPointDialog(jFrame, _pointService);
				addPointDialog.setVisible(true);
				refresh();
			}
		});

		JButton deleteButton = new JButton("Delete Point(s)");

		buttonPanel.add(addButton);
		buttonPanel.add(deleteButton);

		jFrame.add(jList, BorderLayout.CENTER);
		jFrame.add(buttonPanel, BorderLayout.SOUTH);
		jFrame.setVisible(true);
	}

	protected void refresh() {
		_listModel.clear();
		_pointService.getAll()
		             .stream()
		             .forEach(point -> _listModel.addElement(point));
	}

}
