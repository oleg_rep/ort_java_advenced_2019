package ort.adv.task_1.interfaces;

/**
 * @author Oleg Kovalenko
 * 
 * This interface provides method for reading files
 *
 */

public interface Reader {
	/**
	 * 
	 * @param path - it path to file
	 * @return info from file
	 *
	 * Read from file
	 */
	
	String readFile(String path);
}
