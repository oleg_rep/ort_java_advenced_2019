package ort.adv.task_1.interfaces;

import java.util.Scanner;

public interface Writer {

	/**
	 * provides inputing from keyboard
	 */

	void inputData();

	/**
	 * provides writing to file
	 * 
	 * @param data receive some text from keyboard and writing to file
	 */

	void writtingToFile(String data);

	/**
	 * It's getter for field of data
	 * 
	 * @return string from field of data.
	 */

	String getDataBuilder();
}
