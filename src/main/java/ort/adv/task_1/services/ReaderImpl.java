package ort.adv.task_1.services;

import java.util.List;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import ort.adv.task_1.interfaces.Reader;

public class ReaderImpl implements Reader {

	@Override
	public String readFile(String path) {
		StringBuilder infoFromFile = new StringBuilder();
		Path from = Paths.get(path);
		Charset charset = Charset.forName("ISO-8859-1");

		try {
			List<String> lines = Files.readAllLines(from, charset);
			for (String line : lines) {
				infoFromFile.append(line).append("\n");
			}

		} catch (IOException e) {
			System.out.println(e);
		}

		return infoFromFile.toString();
	}

}
