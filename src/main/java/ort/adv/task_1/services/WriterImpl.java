package ort.adv.task_1.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import ort.adv.task_1.interfaces.Writer;

public class WriterImpl implements Writer {

	private StringBuilder dataBuilder = null;
	private String path = null;

	public WriterImpl(StringBuilder dataBuilder, String path) {
		this.dataBuilder = dataBuilder;
		this.path = path;
	}

	public void inputData() {
		Scanner input = new Scanner(System.in);
		String info = null;
		DateFormat infoFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		while (input.hasNextLine()) {
			Calendar cal = Calendar.getInstance();
			String time = infoFormat.format(cal.getTime());
			info = input.nextLine();
			if (info.equals("exit")) {
				break;
			}
			dataBuilder.append(info).append(" : ").append(time).append("\n");
			writtingToFile(dataBuilder.toString());
		}
		input.close();
	}

	public void writtingToFile(String data) {

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
			bw.write(data);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

	public String getDataBuilder() {
		return dataBuilder.toString();
	}
}