package ort.adv.task_1.view;

import java.util.Scanner;

import ort.adv.task_1.interfaces.Reader;
import ort.adv.task_1.interfaces.Writer;
import ort.adv.task_1.services.ReaderImpl;
import ort.adv.task_1.services.WriterImpl;

public class UserInterface {

	private Scanner path;

	{
		path = new Scanner(System.in);
	}

	public void writeText() {
		StringBuilder infoBuilder = new StringBuilder();
		System.out.print("Укажите путь к файлу: ");

		Writer writer = new WriterImpl(infoBuilder, path.nextLine().toString());
		System.out.print("Введите текст: ");
		writer.inputData();
		System.out.println(writer.getDataBuilder());
	}

	public void readText() {
		Reader reader = new ReaderImpl();
		System.out.print("Укажите путь к файлу: ");
		System.out.println("Чтение файла " + reader.readFile(path.nextLine().toString()));
	}

	public void menu() {
		System.out.println("Выберете пункт меню:\n1) Записать в файл \n2) Читать файл");
		Scanner menuItem = new Scanner(System.in);
		switch (menuItem.nextInt()) {
		case 1:
			writeText();
			break;
		case 2:
			readText();
			break;
		default:
			break;
		}
	}
}
