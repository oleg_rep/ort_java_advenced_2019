package com.ort.javaadvanced2019.task_7;

import com.ort.javaadvanced2019.task_7.services.Calculator;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Unit test for simple App.
 */

public class CalculatorTest {

    @Test
    public void plusTest(){
        Calculator calculator = new Calculator();
        int expected = calculator.calc(2, 3,'+');
        int actual = 5;
        assertEquals(expected,actual);
    }

    @Test
    public void minusTest(){
        Calculator calculator = new Calculator();
        int expected = calculator.calc(3, 2,'-');
        int actual = 1;
        assertEquals(expected,actual);
    }

    @Test
    public void multipleTest(){
        Calculator calculator = new Calculator();
        int expected = calculator.calc(3, 2,'*');
        int actual = 6;
        assertEquals(expected,actual);
    }


    @Test
    public void divTest(){
        Calculator calculator = new Calculator();
        int expected = calculator.calc(8, 2,'/');
        int actual = 4;
        assertEquals(expected,actual);
    }
}
