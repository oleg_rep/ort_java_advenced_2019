package ort.adv.Task_1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import ort.adv.task_1.interfaces.Reader;
import ort.adv.task_1.interfaces.Writer;
import ort.adv.task_1.services.ReaderImpl;
import ort.adv.task_1.services.WriterImpl;

public class ReaderWriterTest {

	@Test
	public void writtingToFileTest() {
		StringBuilder builder = new StringBuilder();
		builder.append("Hello : 24/03/2019 21:37:17\n");

		Writer writer = new WriterImpl(builder, null);

		String actual = "Hello : 24/03/2019 21:37:17\n";
		String expected = writer.getDataBuilder();
		assertEquals(expected, actual);
	}

	@Test
	public void readerTest() {
		Reader reader = new ReaderImpl();

		String expected = reader.readFile("PathTest.txt");
		String actual = "Hello : 24/03/2019 21:37:17\n" + "\n";
		assertEquals(expected, actual);
	}
}
